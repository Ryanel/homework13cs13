//===========================================================
// WxController.java
// Purpose: Binds the WxModel to the WxView
// Author: Corwin McKnight
// Updated: May 5, 2019
//===========================================================
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

public class WxController implements Initializable {
    // Model
    WxModel model;

    @FXML
    public Button btnGetWeather;
    
    @FXML
    public TextField txtZipCode;
    
    @FXML
    public Label lblCityName;
    
    @FXML
    public Label lblTemp;
    
    @FXML
    public Label lblWindSpeed;
    
    @FXML
    public Label lblWindDirection;
    
    @FXML
    public Label lblDescription;
    
    @FXML
    public Label lblObservationTime;
    
    @FXML
    public Label lblPressure;
  
    @FXML
    public Label lblHumidity;
    
    @FXML
    private ImageView iconWx;
    
    public WxController() {
        model = new WxModel();
    }
    
    @FXML
    public void handleButtonAction(ActionEvent e) {
        // Get Data from Model
        model.doRequest(txtZipCode.getText());

        // Set data in view
        lblCityName.setText(model.getCityName());
        lblTemp.setText(model.getTemperature());
        lblWindSpeed.setText(model.getWindSpeed());
        lblWindDirection.setText(model.getWindDirection());
        lblDescription.setText(model.getTextDescription());
        lblObservationTime.setText(model.getObservationTime());
        lblHumidity.setText(model.getHumidity());
        lblPressure.setText(model.getPressure());
        iconWx.setImage(model.getImage());
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}