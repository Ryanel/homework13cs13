//===========================================================
// WxMain.java
// Purpose: Starts the application
// Author: Corwin McKnight
// Updated: May 5, 2019
//===========================================================
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class WxMain extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("./WxView.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("Wx - Corwin McKnight");
        stage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}