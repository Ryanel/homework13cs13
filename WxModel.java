//===========================================================
// WxModel.java
// Purpose: Main Model for the OpenWeatherMap API
// Author: Corwin McKnight
// Updated: May 5, 2019
//===========================================================

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;
import java.util.*; 
import java.text.DecimalFormat;
import javafx.scene.image.Image;
import com.google.gson.*;
import java.text.SimpleDateFormat;

public class WxModel {
    private enum ModelState { NoData, InvalidZip, APIRateLimit, Error, ValidData };
    
    private static final String API_KEY = "1191c93470ba92fd6ed8831a4bb5f6e8";
    
    JsonElement jse = null;

    ModelState currentModelState = ModelState.NoData;
    
    public boolean doRequest(String zip) {
        System.out.println("Getting weather data for " + zip);
        
        try {
            // Construct URL
            String urlString = "http://api.openweathermap.org/data/2.5/weather?zip=" + zip + "&APPID=" + API_KEY + "&units=imperial";
            System.out.println("GET " + urlString);
            URL apiRequest = new URL(urlString);
            
            // Make request and recieve data (sync)
            InputStream is = apiRequest.openStream(); // Can throw
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            jse = new JsonParser().parse(br);
            
            is.close();
            br.close();
        }
        catch(java.io.UnsupportedEncodingException uee) {
            uee.printStackTrace();
            return false;
        }
        catch(java.io.IOException ex) {
            currentModelState = ModelState.InvalidZip;
            return false;
        }
        catch(Exception ex) {
            System.out.println("Internal Error: Unknown Exception!");
            ex.printStackTrace();
            currentModelState = ModelState.Error;
            return false;
        }
        if(jse == null) {
            System.out.println("Error: API returned invalid JSON data.");
            currentModelState = ModelState.Error;
            return false;
        }
        // We have JSON data in JSE
        
        // Get Status Code. 200 = good, anything else = bad. 
        String statusCode = jse.getAsJsonObject().get("cod").getAsString();
        
        if(statusCode.equals("200"))
        {
            currentModelState = ModelState.ValidData;
        }
        else if (statusCode.equals("429")) {
            currentModelState = ModelState.APIRateLimit;
        }
        else {
            currentModelState = ModelState.Error;
        }
        
        return true;
    }
    
    public String getCityName() {
        if(currentModelState == ModelState.ValidData) {
            return jse.getAsJsonObject().get("name").getAsString();
        }
        else if (currentModelState == ModelState.InvalidZip) {
            return "Invalid Zipcode";
        }
        else if (currentModelState == ModelState.APIRateLimit) {
            return "API Rate Limited";
        }
        else {
            return "Error";
        }
    }
    
    public String getTemperature() {
        if(currentModelState == ModelState.ValidData) {
            JsonObject mainObj = jse.getAsJsonObject().getAsJsonObject("main");
            Double temp = mainObj.get("temp").getAsDouble();
            DecimalFormat df = new DecimalFormat("#.0");
            return df.format(temp) + "F";
        }
        else {
            return "";
        }
    }
    
    public String getTextDescription() {
        if(currentModelState == ModelState.ValidData) {
            JsonObject weatherObj = jse.getAsJsonObject().getAsJsonArray("weather").get(0).getAsJsonObject();
            String weatherType = weatherObj.get("description").getAsString();
            return capitalize(weatherType);
        }
        else {
            return "";
        }
    }
    
    public String getWindSpeed() {
        if(currentModelState == ModelState.ValidData) {
            JsonObject windObj = jse.getAsJsonObject().getAsJsonObject("wind");
            double windSpd = windObj.get("speed").getAsDouble();
            DecimalFormat df = new DecimalFormat("#.0");
            return df.format(windSpd) + " MPH";
        }
        else {
            return "";
        }
    }
    
    public String getWindDirection() {
        if(currentModelState == ModelState.ValidData) {
            JsonObject windObj = jse.getAsJsonObject().getAsJsonObject("wind");
            double windDeg = windObj.get("deg").getAsDouble();
            return degreesToCompass(windDeg);
        }
        else {
            return "";
        }
    }
    
    public String getPressure() {
        if(currentModelState == ModelState.ValidData) {
            DecimalFormat df = new DecimalFormat("#.00");
        
            JsonObject mainObj = jse.getAsJsonObject().getAsJsonObject("main");
            String pressure = mainObj.get("pressure").getAsString();
            double hg = hPaToInHg(Double.parseDouble(pressure));
            
            return df.format(hg) + " inHg";
        }
        else {
            return "";
        }
    }
    
    public String getHumidity() {
        if(currentModelState == ModelState.ValidData) {
            JsonObject mainObj = jse.getAsJsonObject().getAsJsonObject("main");
            String humidity = mainObj.get("humidity").getAsString();
            return humidity + "%";
        }
        else {
            return "";
        }
    }

    public String getObservationTime() {
        if(currentModelState == ModelState.ValidData) {
            long currentTime = jse.getAsJsonObject().get("dt").getAsLong();
            Date date = new java.util.Date(currentTime * 1000);
            SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMMM d, y 'at' h:mm a, zzzz");
            String formattedDate = sdf.format(date);
            
            return formattedDate;
        }
        else {
            return "";
        }
    }
    
    public Image getImage()
    {
        if (currentModelState == ModelState.ValidData) {
            //String iconURL = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon_url").getAsString();
            JsonObject weatherObj = jse.getAsJsonObject().getAsJsonArray("weather").get(0).getAsJsonObject();
            String weatherIcon = weatherObj.get("icon").getAsString();
            String url = "http://openweathermap.org/img/w/" + weatherIcon + ".png";
            return new Image(url);
        }
        else {
            return new Image("./icons/badzipcode.png");
        }
    }
    
    // Helper, converts hPa to InHg
    private static double hPaToInHg(double hPa) {
        return hPa * 0.02953;
    }
    
    // Helper, capitalizes first letter of a sentance
    private String capitalize(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }
    
    // Helper, takes degrees and converts to Compas
    public static String degreesToCompass(double x)
    {
        String directions[] = {"N", "NE", "E", "SE", "S", "SW", "W", "NW", "N"};
        return directions[(int)Math.round((((double)x % 360) / 45))];
    }
}